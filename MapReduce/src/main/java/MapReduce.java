
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.io.DataOutput;
import java.io.DataInput;

import org.apache.hadoop.conf.Configuration;
import java.net.URI;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.SnappyCodec;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile.CompressionType;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;



public class MapReduce
{

	//Класс маппера
	public static class VMapper extends Mapper<Object, Text, VCityIDWritable, IntWritable>
	{
		private final static IntWritable one = new IntWritable( 1 );
		private final static int MIN_PARAMS = 17;
		private final static int CITY_ID_OFF = 16;
		private final static int PRICE_OFF = 4;
		private final static int MIN_PRICE = 250;
		//private static final Log log = LogFactory.getLog(VMapper.class);
		
		public void map( Object key, Text value, Context context ) throws IOException, InterruptedException, NumberFormatException 
		{
			//Разделение строки на параметры.
			//Параметры разделены пробеломи.
			
			String[] par = value.toString().split("\\s+");

			int len = par.length;
			int price;
			int city_id;
			
			//Проверка колличества параметров
			if( len < MIN_PARAMS )
				return;
			
			try
			{
				//Получение id города и цены
				price = Integer.parseInt( par[len-1-PRICE_OFF] );
				city_id = Integer.parseInt( par[len-1-CITY_ID_OFF] );
			}
			catch(NumberFormatException e)
			{
				return;
			}
			
			//Проверка минимальной цены
			if( price > MIN_PRICE )
			{
				context.write( new VCityIDWritable( city_id ), one );
			}
		}
		
	}

	
	//Класс комбайнера
	public static class VCombiner extends Reducer<VCityIDWritable, IntWritable, VCityIDWritable, IntWritable>
	{
		private IntWritable result = new IntWritable();

		public void reduce( VCityIDWritable key, Iterable<IntWritable> values, Context context ) throws IOException, InterruptedException
		{
			//Нахождения колличества операций на город.
			//Данное колличество является промежуточным.
			
			int sum = 0;
			
			for( IntWritable val : values )
			{
				sum += val.get();
			}
			
			result.set( sum );
			
			context.write( key , result );
		}		
	}
	
	
	//Класс редьюсера
	public static class VReducer extends Reducer<VCityIDWritable, IntWritable, Text, IntWritable>
	{
		private IntWritable result = new IntWritable();
		private HashMap<Integer, String> city = new HashMap<Integer, String>();
		//private String CityFilePaths[];
	
		
		public void setup( Context context ) throws IOException, InterruptedException
		{
			//Считываем данные о названии городов
			city.put( 0, "unknown" );
			
			try
			{
				URI[] cacheFilesLocal = context.getCacheFiles();
				for( URI eachUri : cacheFilesLocal )
				{
					readCity( "", eachUri.toString(), context );
				}
			}catch (NullPointerException e) {
				readCity( "", "../input/city.en.txt", context );
				readCity( "", "../input/region.en.txt", context );
			}
		}
		
		
		public void setCustonPaths( String city, String region )
		{
			//CityFilePaths.clear();
			//CityFilePaths.add(city);
			//CityFilePaths.add(region);
		}
		
		
		private void readCity( String pref, String p, Context context ) throws IOException, InterruptedException
		{
			Path path = new Path( p );
			FileSystem fs = FileSystem.get( context.getConfiguration() );
			BufferedReader br = new BufferedReader( new InputStreamReader( fs.open(path) ) );
			
			try
			{
				String line;
				line=br.readLine();
				while ( line != null )
				{
					String[] par = line.split("\\s+");
					int id = Integer.parseInt( par[0] );
					
					if( !city.containsKey( id ) )
						city.put( id, pref+par[1] );					
					
					line = br.readLine();
				}
			}
			finally
			{
				br.close();
			}				
		}
				

		public void reduce( VCityIDWritable key, Iterable<IntWritable> values, Context context ) throws IOException, InterruptedException
		{
			//Нахождения колличества операций на город.
			
			int sum = 0;
			
			for( IntWritable val : values )
			{
				sum += val.get();
			}
			
			result.set( sum );
			
			if( city.containsKey( key.get() ) )
				context.write( new Text( city.get( key.get() ) ) , result );
			else
				context.write( new Text( "noname["+key.toString()+"]" ) , result );
		}
	}
	
	

	public static void main( String[] args ) throws Exception
	{
		Configuration conf = new Configuration();
		conf.set(TextOutputFormat.SEPERATOR, ";");
		//Configuration conf = this.getConf();
		conf.set("mapred.textoutputformat.separator", ";"); //Prior to Hadoop 2 (YARN)
		//conf.set("mapreduce.textoutputformat.separator", ";");  //Hadoop v2+ (YARN)
		//conf.set("mapreduce.output.textoutputformat.separator", ";");
		//conf.set("mapreduce.output.key.field.separator", ";");
		//conf.set("mapred.textoutputformat.separatorText", ";"); // ?*/
		
		Job job = Job.getInstance( conf, "map reduce" );
		
		job.setJarByClass( MapReduce.class );
		job.setMapperClass( VMapper.class );
		job.setCombinerClass( VCombiner.class );
		job.setReducerClass( VReducer.class );
		
		job.setMapOutputKeyClass( VCityIDWritable.class );
		job.setMapOutputValueClass( IntWritable.class );	
		job.setOutputKeyClass( Text.class );
		job.setOutputValueClass( IntWritable.class );
		
		//job.setOutputFormat(TextOutputFormat.class);
		job.setOutputFormatClass( TextOutputFormat.class );

		FileInputFormat.addInputPath( job, new Path(args[0]) );
		FileOutputFormat.setOutputPath( job, new Path(args[1]) );
		
		job.addCacheFile(new URI("/map_reduce/input/city.en.txt"));
		job.addCacheFile(new URI("/map_reduce/input/city.en.txt"));
		
		//SequenceFileOutputFormat.setOutputPath( job, new Path(args[1]) );
		//SequenceFileOutputFormat.setCompressOutput( job, true );
		//SequenceFileOutputFormat.setOutputCompressorClass( job, SnappyCodec.class );
		
		System.exit( job.waitForCompletion(true) ? 0 : 1 );
	}
  
  
  
}









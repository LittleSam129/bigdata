
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.Text;

import org.junit.Before;
import org.junit.Test;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;


public class VReducerTest
{

    ReduceDriver<VCityIDWritable, IntWritable, Text, IntWritable> redDriver;

    @Before
    public void setUp()
	{
        MapReduce.VReducer reducer = new MapReduce.VReducer();
		reducer.setCustonPaths( "../input/city.en.txt",
								"../input/region.en.txt");
        redDriver = ReduceDriver.newReduceDriver(reducer);
    }

    @Test
    public void testReduceNormal() throws IOException
	{
		List<IntWritable> v0 = new ArrayList<IntWritable>();
		v0.add(new IntWritable(1));
		v0.add(new IntWritable(1));
		v0.add(new IntWritable(1));
		
		List<IntWritable> v1 = new ArrayList<IntWritable>();
		v1.add(new IntWritable(1));
		
		List<IntWritable> v2 = new ArrayList<IntWritable>();
		v2.add(new IntWritable(1));
		v2.add(new IntWritable(1));
		
		List<IntWritable> v3 = new ArrayList<IntWritable>();
		v3.add(new IntWritable(1));
		v3.add(new IntWritable(1));		
		
		redDriver.withInput(new VCityIDWritable(0), v0);
		redDriver.withInput(new VCityIDWritable(206), v1);
		redDriver.withInput(new VCityIDWritable(325), v2);
		redDriver.withInput(new VCityIDWritable(999), v3);
		
		redDriver.withOutput(new Text("unknown"), new IntWritable(3));
		redDriver.withOutput(new Text("shaoyang"), new IntWritable(1));
		redDriver.withOutput(new Text("xizang"), new IntWritable(2));
		redDriver.withOutput(new Text("noname[999]"), new IntWritable(2));	

		redDriver.runTest();		
    }
	
	
}
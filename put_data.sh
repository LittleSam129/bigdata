#!/bin/bash

hadoop fs -mkdir /map_reduce
hadoop fs -mkdir /map_reduce/input
hadoop fs -rm /map_reduce/input/*
hadoop fs -put ./input/* /map_reduce/input

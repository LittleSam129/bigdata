#!/bin/bash

hadoop fs -rm -r /map_reduce/output

echo "[HADOOP START]"


#hadoop jar MapReduce.jar MapReduce /map_reduce/input/imp.*.txt /map_reduce/output
hadoop jar MapReduce/target/1-1.0-SNAPSHOT.jar MapReduce /map_reduce/input/imp.*.txt /map_reduce/output


if [ "$?" -ne "0" ];
then
	echo "<<<HADOOP ERROR>>>"
	exit

fi


echo "[HADOOP FINISH]"
rm ./output/*
#hadoop cat /map_reduce/output/part-r-* >> ./output/results.txt
hadoop fs -get /map_reduce/output/* ./output
hadoop dfs -text /map_reduce/output/part-r-* >> ./output/results.txt


echo "[RESULTS]"

cat ./output/results.txt


echo "[SUCESS]"

